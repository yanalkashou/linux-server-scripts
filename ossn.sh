#!/bin/bash

DOMAIN=$1
if [ -z $1 ]
then
echo ""
printf "Enter the domain you want to host OSSN and press [ENTER]\nExamples: my-site.com or docs.my-site.com\n"
read DOMAIN
fi

apt-get install lamp-server^ -y
apt-get install php7.0-cli php7.0-common php7.0-json php7.0-mcrypt php7.0-mysql php7.0-xml php7.0-curl php7.0-zip php7.0-gd -y
# add-apt-repository ppa:ondrej/php
# apt-get update
# apt-get install php7.0 php7.0-mysql php7.0-curl php7.0-json php7.0-cgi libapache2-mod-php7.0 php7.0-mcrypt php7.0-xmlrpc php7.0-gd php7.0-mbstring php7.0 php7.0-common php7.0-xmlrpc php7.0-soap php7.0-xml php7.0-intl php7.0-cli php7.0-ldap php7.0-zip php7.0-readline php7.0-imap php7.0-tidy php7.0-recode php7.0-sq php7.0-intl
apt-get install unzip

a2enmod rewrite 
systemctl restart apache2 

wget https://www.opensource-socialnetwork.org/downloads/ossn-v4.2-1468404691.zip -O ossn.zip 
unzip ossn.zip -d /var/www/html/ 

mysql -u root -p 
SET GLOBAL sql_mode=' '  
CREATE DATABASE ossn_db 
CREATE USER ' ossn_user' @' localhost'  IDENTIFIED BY ' 123' 
GRANT ALL PRIVILEGES ON ossn_db.* TO ' ossn_user' @' localhost' 
FLUSH PRIVILEGES
exit

nano /etc/php/7.0/cli/php.ini 
# unzip build.zip

# mv ossn /var/www/ &

mkdir -p /var/www/ossndatadir 
chown www-data:www-data -R /var/www/html/ossn/ 
touch /etc/apache2/sites-available/ossn.conf
ln -s /etc/apache2/sites-available/ossn.conf /etc/apache2/sites-enabled/ossn.conf


# cd /var/www/ &
# chown -R www-data.www-data ossn &
# chmod -R 775 ossn

cat >/etc/apache2/sites-available/ossn.conf <<EOL
<VirtualHost *:80> 
    ServerName ${DOMAIN}
    DocumentRoot /var/www/html/ossn/

    <Directory /var/www/html/ossn/> 
        Options FollowSymLinks
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory> 
    ErrorLog /var/log/apache2/linuxhelp.com-error_log
    CustomLog /var/log/apache2/linuxhelp.com-access_log common
</VirtualHost> 
EOL

systemctl restart apache2

# a2ensite ossn.conf
# a2dissite 000-default.conf
# a2enmod rewrite

# systemctl restart apache2